<div style="height: 100vh">
  <div class="flex-center flex-column">
    
    <h3 class="mb-5">Controle Financeiro Pessoal</h3>
    
    <form class="text-center border border-light p-5" method="POST">
      
      <div class="form-outline mb-4">
        <p class="h4 md-4">Login</p>

        <input type="email" id="email" name="email" class="form-control md-4" />
        <label class="form-label" for="form2Example1">Email</label>
      </div>

      
      <div class="form-outline mb-4">
        <input type="password" id="senha" name="senha" class="form-control mb-4" />
        <label class="form-label" for="form2Example2">Password</label>
      </div>

      <button type="submit" class="btn btn-primary btn-block mb-4">Enviar</button>
      
      <p class="red-text"><?= $error ? 'Dados de acesso Incorretos.' : '' ?></p>
    </form>

  </div>
</div>