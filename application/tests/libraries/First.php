<?php

class First extends TestCase{
    
    public function setUp(): void {

        $this->resetInstance();

    }

    function testContaDeveInserirCorretamente(){

        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        $this->CI->load->library('conta');
        $res = $this->CI->conta->lista('pagar', 1, 2021);

        $this->assertEquals(3, sizeof($res));
        $this->assertEquals(2021, $res[0]['ano']);
        $this->assertEquals(1, $res[0]['mes']);
        $this->assertEquals('magalu', $res[0]['parceiro']);

        $this->assertEquals(2021, $res[2]['ano']);
        $this->assertEquals(1, $res[2]['mes']);
        $this->assertEquals('Casas', $res[2]['parceiro']);

        $this->assertEquals(97, $res['valor']);
        $this->assertEquals(2021, $res[2]['ano']);
        $this->assertEquals(1, $res[2]['mes']);
        $this->assertEquals('energia', $res[2]['parceiro']);
    }

    function testContaInformaTotalDeContas(){
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        $this->CI->load->library('conta');
        $res = $this->CI->conta->total('pagar', 1, 2021);

        $this->assertEquals(5097, $res);
    }

    function testContaDeveCalcularSaldoMensal(){
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        $this->CI->load->library('conta');
        $this->CI->conta->saldo(1, 2021);

        $this->assertEquals(-875, $res);
    }

}