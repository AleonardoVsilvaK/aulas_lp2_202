<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/CI_Object.php';

class ContaDataBuilder extends CI_Object {

    private $contas = [
        [
            'parceiro' => 'Magalu',
            'descricao' => 'notebook',
            'valor' => '2000',
            'mes' => 1,
            'ano' => 2021, 
            'tipo' => 'pagar'
        ],
        [
            'parceiro' => 'Casas',
            'descricao' => 'notebook',
            'valor' => '4000',
            'mes' => 1,
            'ano' => 2021, 
            'tipo' => 'pagar'
        ],
        [
            'parceiro' => 'Salário',
            'descricao' => 'Prefeitura',
            'valor' => '3500',
            'mes' => 1,
            'ano' => 2021, 
            'tipo' => 'receber'
        ],
        [
            'parceiro' => 'Aluguel',
            'descricao' => 'Casa',
            'valor' => '680',
            'mes' => 1,
            'ano' => 2021, 
            'tipo' => 'receber'
        ],
        [
            'parceiro' => 'Energia',
            'descricao' => 'notebook',
            'valor' => '97',
            'mes' => 1,
            'ano' => 2021, 
            'tipo' => 'pagar'
        ],
    ];

    public function start(){
        $this->load->library('conta');

        foreach($this->$contas as $conta){
            $this->conta->cria($conta);
        }
    }

    public function clear(){
        $this->db->truncate('conta');
    }

}
